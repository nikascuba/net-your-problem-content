# net-your-problem-content

## Content Authoring

Visit https://admin.netyourproblem.com to manage content for the main site. You will need to have `write` access to this repository in order to login to the content authoring site.

### FAQs about the CMS

**Q**: What are collections?

**A**: When you login into the content manager, the items you see on the left nav menu are called the collections.

**Q**: How do I add new cards to Get Involved, Projects, and the News sections?

**A**: Go to the Lists collection in the content manager. Then pick the appropriate section and add the new entry. Don't forget to add a corresponding entry under the main collection for the Get Involved and Projects. Otherwise, when the user clicks on Read More, nothing will show up in the popup.

**Q**: Why doesn't the News section have a main collection of its own like the Get Involved and Projects sections?

**A**: That's because the cards in the News section are mainly references to external news articles about Net Your Problem. That's why the News section only exists withing the Lists collection. You will simply provide a title, a cover image, a brief description and a link to the actual news article.

**Q**: How do I make sure my cards appear in an order that I want?

**A**: Simply drag/drop to re-arrange the cards in the content manager when you are in the corresponding Lists collection.

**Q**: I made edits to some content and I verified them in the dev site. What do I do now in order to see the changes in the production/live site?

**A**: Assuming you are reading this FAQ from within the Bitbucket site and in the `net-your-problem-content` repository. 
   - Click on the `Pull Requests` nav item on the left side.

   - Then click on the `create a pull request` link or the `Create pull request` button. Both perform the same action so clickin on either is fine.

   - From the **left** drop-down, select `dev`, and from the right drop-down, select `master`.

   - You are basically bringing the content changes from the dev branch to the master branch. This is what allows you to publish your changes to the live site.

   - Click `Create Pull Request`.

   - Wait! You are not done yet.

   - Now that the pull request has been created, you can now navigate to it (if you aren't already there) from the Pull Requests link you clicked earlier, and click on `Approve` and then click on `Merge`.

   - Follow the prompts, and finish the merge.
   
   - That's it. You are done now. You should see the changes in the live site.

**Q**: What is the `Detailed Information FileName` in the Lists > Projects collection? What should I enter as the value?

**A**: This is the name of the file that will contain the detailed project information, which will be shown in the pop-up when someone clicks on the "Read More" button of a project's card. You absolutely **need** to enter a value for this. *For example*, if the title of your project is "Dutch Harbor, Alaska", then the value of this should be "dutch-harbor-alaska". Basically, you should replace whitespaces and any punctuation (like comma) with a dash `-`. Then, when you create the entry for this project under the main Projects collection, be sure to enter the title exactly as you did in the Lists collection for the project.

## Project setup (for development purposes)
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Production

The production build will auto-deploy from the `master` branch and the updates will be available at https://admin.netyourproblem.com.

> Be careful merging `dev` to `master` because there may be content updates in `dev` that should not be merged yet. So use a separate branch for updates to the Netlify CMS and Vue itself and merge that separately to master.

If you added CMS changes to the `dev` branch, but can't merge to `master` yet, run this project locally using `npm run serve` and make changes to the content that way. Any content changes would then be published to the `dev` branch in the Bitbucket repo. The main website project (`net-your-problem-vue`) can then be run locally as well, which would read content from the `dev` branch.